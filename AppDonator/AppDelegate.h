//
//  AppDelegate.h
//  AppDonator
//
//  Created by Predrag Samardzic on 19/08/15.
//  Copyright (c) 2015 pedja. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property NSArray* charities;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

