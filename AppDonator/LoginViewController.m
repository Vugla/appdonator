//
//  LoginViewController.m
//  AppDonator
//
//  Created by Predrag Samardzic on 22/08/15.
//  Copyright (c) 2015 pedja. All rights reserved.
//

#import "LoginViewController.h"


@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet FBSDKLoginButton *fbLogin;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.fbLogin.readPermissions = @[@"public_profile", @"email", @"user_friends"];
    self.fbLogin.delegate=self;
 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error{
    
        [self performSegueWithIdentifier:@"toMain" sender:self];
    
    
}

-(void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton{
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
