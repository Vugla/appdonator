//
//  DetailsViewController.h
//  AppDonator
//
//  Created by Predrag Samardzic on 22/08/15.
//  Copyright (c) 2015 pedja. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Charity.h"
#import <MessageUI/MFMessageComposeViewController.h>
#import <Braintree/Braintree.h>

@interface DetailsViewController : UIViewController<UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,MFMessageComposeViewControllerDelegate,BTDropInViewControllerDelegate>
@property Charity* charity;
@end
