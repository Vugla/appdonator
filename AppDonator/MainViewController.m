//
//  MainViewController.m
//  AppDonator
//
//  Created by Predrag Samardzic on 21/08/15.
//  Copyright (c) 2015 pedja. All rights reserved.
//

#import "MainViewController.h"
#import "AppDelegate.h"
#import "Charity.h"
#import "DetailsViewController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface MainViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) NSFetchRequest *searchFetchRequest;
@end

@implementation MainViewController

NSInteger selectedRow;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithRed:0/255.0f green:85/255.0f blue:155/255.0f alpha:1];
    self.tableView.backgroundColor = [UIColor colorWithRed:0/255.0f green:85/255.0f blue:155/255.0f alpha:1];
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    self.definesPresentationContext = YES;
    self.searchController.hidesNavigationBarDuringPresentation = false;
    self.searchController.searchBar.barTintColor=[UIColor colorWithRed:0/255.0f green:85/255.0f blue:155/255.0f alpha:1];
    self.searchController.searchBar.tintColor=[UIColor colorWithRed:255/255.0f green:206/255.0f blue:0/255.0f alpha:1];
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    

    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         
         if (!error) {
          
             if (result[@"name"]) {
                 self.titleLabel.text = [NSString stringWithFormat:@"Hello %@, please choose a charity for your donation", result[@"name"]];
             }else if ([[FBSDKProfile currentProfile]firstName] && [[FBSDKProfile currentProfile]lastName]){
                 self.titleLabel.text = [NSString stringWithFormat:@"Hello %@ %@, please choose a charity for your donation", [[FBSDKProfile currentProfile]firstName], [[FBSDKProfile currentProfile]lastName]];
             }else if ([[FBSDKProfile currentProfile]firstName]){
                 self.titleLabel.text = [NSString stringWithFormat:@"Hello %@, please choose a charity for your donation", [[FBSDKProfile currentProfile]firstName]];
             }
             

         }else if ([[FBSDKProfile currentProfile]firstName] && [[FBSDKProfile currentProfile]lastName]){
             self.titleLabel.text = [NSString stringWithFormat:@"Hello %@ %@, please choose a charity for your donation", [[FBSDKProfile currentProfile]firstName], [[FBSDKProfile currentProfile]lastName]];
         }else if ([[FBSDKProfile currentProfile]firstName]){
             self.titleLabel.text = [NSString stringWithFormat:@"Hello %@, please choose a charity for your donation", [[FBSDKProfile currentProfile]firstName]];
         }
     }];

    self.fbPic.layer.cornerRadius = 30.0;
    self.fbPic.clipsToBounds=YES;
    
    [self searchForText:@""];
    // Do any additional setup after loading the view.
}



- (void)didReceiveMemoryWarning {
    self.searchFetchRequest = nil;
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
   return [self.filteredList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CharityCell" forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor whiteColor];
    Charity* charity = [self.filteredList objectAtIndex:indexPath.row];
    
    cell.textLabel.text = charity.name;
    cell.textLabel.textColor = [UIColor colorWithRed:0/255.0f green:85/255.0f blue:155/255.0f alpha:1];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    selectedRow = indexPath.row;
    [self performSegueWithIdentifier:@"toDetails" sender:self];
}

#pragma mark - Search

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = searchController.searchBar.text;
    [self searchForText:searchString];
    [self.tableView reloadData];
}

- (void)searchForText:(NSString *)searchText
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (appDelegate.managedObjectContext)
    {
        NSString *predicateFormat = @"%K BEGINSWITH[cd] %@";
        NSString *searchAttribute = @"name";
        
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFormat, searchAttribute, searchText];
        [self.searchFetchRequest setPredicate:predicate];
        
        NSError *error = nil;
         if ([searchText length] > 0){
            self.filteredList = [appDelegate.managedObjectContext executeFetchRequest:self.searchFetchRequest error:&error];
         }else{
             self.filteredList = appDelegate.charities;
         }
        
         }
}

- (NSFetchRequest *)searchFetchRequest
{
    
    if (_searchFetchRequest != nil)
    {
        return _searchFetchRequest;
    }
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    _searchFetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Charity" inManagedObjectContext:appDelegate.managedObjectContext];
    [_searchFetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    
    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
    [_searchFetchRequest setSortDescriptors:sortDescriptors];
    
    return _searchFetchRequest;
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"toDetails"]) {
        
        DetailsViewController* controller = segue.destinationViewController;
        controller.charity = [self.filteredList objectAtIndex:selectedRow];
    }
}




@end
