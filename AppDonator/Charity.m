//
//  Charity.m
//  AppDonator
//
//  Created by Predrag Samardzic on 21/08/15.
//  Copyright (c) 2015 pedja. All rights reserved.
//

#import "Charity.h"
#import "CharityCategory.h"


@implementation Charity

@dynamic bankPaymentAccountNumber;
@dynamic bankPaymentSortCode;
@dynamic city;
@dynamic country;
@dynamic email;
@dynamic id;
@dynamic info;
@dynamic name;
@dynamic premiumVoiceCallPhone;
@dynamic smsPaymentPhone;
@dynamic smsPaymentText;
@dynamic updatedAt;
@dynamic website;
@dynamic charityToCategory;

@end
