//
//  CharityCategory.h
//  AppDonator
//
//  Created by Predrag Samardzic on 21/08/15.
//  Copyright (c) 2015 pedja. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Charity;

@interface CharityCategory : NSManagedObject

@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *charityToCategory;
@end

@interface CharityCategory (CoreDataGeneratedAccessors)

- (void)addCharityToCategoryObject:(Charity *)value;
- (void)removeCharityToCategoryObject:(Charity *)value;
- (void)addCharityToCategory:(NSSet *)values;
- (void)removeCharityToCategory:(NSSet *)values;

@end
