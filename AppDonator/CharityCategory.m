//
//  CharityCategory.m
//  AppDonator
//
//  Created by Predrag Samardzic on 21/08/15.
//  Copyright (c) 2015 pedja. All rights reserved.
//

#import "CharityCategory.h"
#import "Charity.h"


@implementation CharityCategory

@dynamic id;
@dynamic name;
@dynamic charityToCategory;

@end
