//
//  Charity.h
//  AppDonator
//
//  Created by Predrag Samardzic on 21/08/15.
//  Copyright (c) 2015 pedja. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CharityCategory;

@interface Charity : NSManagedObject

@property (nonatomic, retain) NSString * bankPaymentAccountNumber;
@property (nonatomic, retain) NSString * bankPaymentSortCode;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * country;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSString * info;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * premiumVoiceCallPhone;
@property (nonatomic, retain) NSString * smsPaymentPhone;
@property (nonatomic, retain) NSString * smsPaymentText;
@property (nonatomic, retain) NSDate * updatedAt;
@property (nonatomic, retain) NSString * website;
@property (nonatomic, retain) NSSet *charityToCategory;
@end

@interface Charity (CoreDataGeneratedAccessors)

- (void)addCharityToCategoryObject:(CharityCategory *)value;
- (void)removeCharityToCategoryObject:(CharityCategory *)value;
- (void)addCharityToCategory:(NSSet *)values;
- (void)removeCharityToCategory:(NSSet *)values;

@end
