//
//  DetailsViewController.m
//  AppDonator
//
//  Created by Predrag Samardzic on 22/08/15.
//  Copyright (c) 2015 pedja. All rights reserved.
//

#import "DetailsViewController.h"
#import "BankTransferViewController.h"
#import "VoiceCallViewController.h"
#import "AFHTTPRequestOperationManager.h"

#define PAYMENT_SERVER @"http://localhost:3000/payments/"

@interface DetailsViewController ()
@property (nonatomic, strong) Braintree *braintree;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *amountTextField;
@property (weak, nonatomic) IBOutlet UILabel *middleLabel;
@property (weak, nonatomic) IBOutlet UITextField *paymentMethodTextField;
@property (weak, nonatomic) IBOutlet UILabel *toLabel;

@property (weak, nonatomic) IBOutlet UIButton *donateButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *scrollViewContent;
@property (weak, nonatomic) IBOutlet UIImageView *charityImage;
@property (weak, nonatomic) IBOutlet UILabel *charityName;
@property (weak, nonatomic) IBOutlet UILabel *charityInfo;
@property (weak, nonatomic) IBOutlet UIButton *paymentMethodButton;
@property UITextField* currentTextField;
@end

@implementation DetailsViewController

NSArray* paymentOptions;
UIPickerView* picker;
NSInteger selectedPaymentMethod;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    picker = nil;
    //available payment options for picker view
    paymentOptions = [[NSArray alloc]initWithObjects:@"SMS",@"Credit Card",@"Voice Call",@"Bank Transfer", nil];
    selectedPaymentMethod = 0;
    
     self.view.backgroundColor = [UIColor colorWithRed:0/255.0f green:85/255.0f blue:155/255.0f alpha:1];
    self.titleLabel.textColor = [UIColor colorWithRed:255/255.0f green:206/255.0f blue:0/255.0f alpha:1];
    self.middleLabel.textColor = [UIColor colorWithRed:255/255.0f green:206/255.0f blue:0/255.0f alpha:1];
    self.toLabel.textColor = [UIColor colorWithRed:255/255.0f green:206/255.0f blue:0/255.0f alpha:1];
    
    [self.paymentMethodButton setBackgroundImage:[[UIImage imageNamed:@"dropdown_normal_stretchable.png"] stretchableImageWithLeftCapWidth:7 topCapHeight:0] forState:UIControlStateNormal];

   
    self.paymentMethodTextField.backgroundColor = [UIColor clearColor];
    [self.view bringSubviewToFront:self.paymentMethodTextField];
    
    //populating charity's info
    self.charityName.text = self.charity.name;
    self.charityInfo.text = self.charity.info;
    
   
    
    //adding tap recognizer in order to dismiss keyboard when user taps outside of it
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [tap setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tap];
    
    //setting delegate for textfields
    self.paymentMethodTextField.delegate = self;
    self.amountTextField.delegate = self;

    self.amountTextField.keyboardType = UIKeyboardTypeNumberPad;
    
    // TODO: Switch this URL to your own authenticated API
    NSURL *clientTokenURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@client_token", PAYMENT_SERVER] ];
    NSMutableURLRequest *clientTokenRequest = [NSMutableURLRequest requestWithURL:clientTokenURL];
    [clientTokenRequest setValue:@"text/plain" forHTTPHeaderField:@"Accept"];
    
    [NSURLConnection
     sendAsynchronousRequest:clientTokenRequest
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
         // TODO: Handle errors in [(NSHTTPURLResponse *)response statusCode] and connectionError
         NSString *clientToken = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
         
         if ([clientToken length ]>0) {
             self.braintree = [Braintree braintreeWithClientToken:clientToken];
         }
         // Initialize `Braintree` once per checkout session
         
         
         // As an example, you may wish to present our Drop-In UI at this point.
         // Continue to the next section to learn more...
     }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    //setting content size for scrollView
    [self.scrollView setContentSize:self.scrollViewContent.frame.size];
}

-(void)dismissKeyboard{
    [self.currentTextField resignFirstResponder];

}

//actions for pickerView's toolbar buttons
- (IBAction)doneButtonPressed:(id)sender{
    
   [self.paymentMethodTextField setText:[paymentOptions objectAtIndex:selectedPaymentMethod]];
    [self dismissKeyboard];
}

- (IBAction)cancelButtonPressed:(id)sender{
    
    [self dismissKeyboard];
}

#pragma mark UITextFieldDelegate

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    self.currentTextField = textField;
    
    //set pickerView as inputView instead of keyboard when user wants to change payment method
    if (textField == self.paymentMethodTextField) {
        
        if (!picker) {
            picker = [[UIPickerView alloc]init];
            picker.delegate = self;
            picker.dataSource = self;
            picker.backgroundColor = [UIColor whiteColor];
            
        }
        
        //create a toolbar with done and cancel button
        UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,44)];
        
        toolBar.backgroundColor = [UIColor colorWithRed:255/255.0f green:206/255.0f blue:0/255.0f alpha:1];
        toolBar.tintColor = [UIColor colorWithRed:0/255.0f green:85/255.0f blue:155/255.0f alpha:1];
        UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                          style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonPressed:)];
        UIBarButtonItem *barButtonCancel = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                          style:UIBarButtonItemStyleDone target:self action:@selector(cancelButtonPressed:)];
        
        [toolBar setItems:[NSArray arrayWithObjects:barButtonCancel,flexibleSpace, barButtonDone, nil]];

        textField.inputView = picker;
        textField.inputAccessoryView = toolBar;

    }
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    //prevent user input outside of pickerView for paymentOption
    if (textField == self.paymentMethodTextField) {
         return NO;
    }else if (!string.length) //allow backspace
        {
            return YES;
        }
    
    // Prevent invalid character input, if keyboard is numberpad
    else if (textField.keyboardType == UIKeyboardTypeNumberPad)
    {
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
        {
            return NO;
        }
        // verify max length has not been exceeded
        NSString *updatedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        if (updatedText.length > 10) //
        {
            // suppress the max length message only when the user is typing
            // easy: pasted data has a length greater than 1; who copy/pastes one character?
            if (string.length > 1)
            {
                // BasicAlert(@"", @"This field accepts a maximum of 4 characters.");
                [self showMessage:@"This field accepts a maximum of 10 digits" withTitle:@""];
            }
            
            return NO;
        }
        
        return YES;
    }else return YES;
}


#pragma mark - UIPickerView

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [paymentOptions count];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return [paymentOptions objectAtIndex:row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    selectedPaymentMethod = row;
//    [self updateButtonForRow:row];
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"toBankTransfer"]) {
        BankTransferViewController* controller = segue.destinationViewController;
        controller.name = self.charity.name;
        controller.sortCode = self.charity.bankPaymentSortCode;
        controller.accountNumber = self.charity.bankPaymentAccountNumber;
    }
    if ([segue.identifier isEqualToString:@"toVoiceCall"]) {
        VoiceCallViewController* controller = segue.destinationViewController;
        controller.name = self.charity.name;
        controller.phoneNumber = self.charity.premiumVoiceCallPhone;
    }
   
}


// Show an alert message
- (void)showMessage:(NSString *)text withTitle:(NSString *)title
{
   
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:text
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert.view setTintColor:[UIColor colorWithRed:0/255.0f green:85/255.0f blue:155/255.0f alpha:1]];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)paymentMethodButtonPressed:(id)sender {
    
    [self.paymentMethodTextField becomeFirstResponder];
    
}

- (IBAction)donatePressed:(id)sender {
    
    if ([self.paymentMethodTextField.text isEqualToString:@"SMS"]) {
    if (![self.amountTextField.text integerValue]>0) {
        [self showMessage:@"Please specify amount to donate" withTitle:@"No amount specified"];
    }else{
        if ([self.charity.smsPaymentText length]>0) {
            
            [self sendSMS:[NSString stringWithFormat:@"%@ %@",self.charity.smsPaymentText,self.amountTextField.text] recipientList:[NSArray arrayWithObjects:self.charity.smsPaymentPhone, nil]];
        }else{
            [self sendSMS:self.amountTextField.text recipientList:[NSArray arrayWithObjects:self.charity.smsPaymentPhone, nil]];
        }
        }
    }else if([self.paymentMethodTextField.text isEqualToString:@"Bank Transfer"]){
        [self performSegueWithIdentifier:@"toBankTransfer" sender:self];
    }else if ([self.paymentMethodTextField.text isEqualToString:@"Voice Call"]){
        [self performSegueWithIdentifier:@"toVoiceCall" sender:self];
    }else if ([self.paymentMethodTextField.text isEqualToString:@"Credit Card"]){
        
        if ([self.amountTextField.text integerValue] >= 5) {
            // Create a BTDropInViewController
            if (self.braintree) {
                BTDropInViewController *dropInViewController = [self.braintree dropInViewControllerWithDelegate:self];
                // This is where you might want to customize your Drop in. (See below.)
                
                // The way you present your BTDropInViewController instance is up to you.
                // In this example, we wrap it in a new, modally presented navigation controller:
                dropInViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                                                      target:self
                                                                                                                      action:@selector(userDidCancelPayment)];
                
                UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:dropInViewController];
                [self presentViewController:navigationController animated:YES completion:nil];
            }else{
                [self showMessage:@"Check your internet connection or try restarting the app." withTitle:@"An error occured"];
            }
           
        }else{
            [self showMessage:@"Need to donate at least 5€ with this method" withTitle:nil];
        }
               }
        
    
}

- (void)sendSMS:(NSString *)bodyOfMessage recipientList:(NSArray *)recipients
{
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        controller.body = bodyOfMessage;
        controller.recipients = recipients;
        controller.messageComposeDelegate = self;
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if (result == MessageComposeResultCancelled)
        NSLog(@"Message cancelled");
        else if (result == MessageComposeResultSent)
            NSLog(@"Message sent");
            else 
                NSLog(@"Message failed")  ;
                }

- (void)userDidCancelPayment {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark BTDropInViewControllerDelegate

- (void)dropInViewController:(__unused BTDropInViewController *)viewController didSucceedWithPaymentMethod:(BTPaymentMethod *)paymentMethod {
    [self postNonceToServer:paymentMethod.nonce]; // Send payment method nonce to your server
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)dropInViewControllerDidCancel:(__unused BTDropInViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)postNonceToServer:(NSString *)paymentMethodNonce {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameters = @{@"payment_method_nonce": paymentMethodNonce,
                                 @"amount":self.amountTextField.text,
                                 @"charity":self.charity.name};
    [manager POST:[NSString stringWithFormat:@"%@%@",PAYMENT_SERVER,@"payment-methods"] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        [self showMessage:@"Thank you for donating" withTitle:@"Transaction successful"];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [self showMessage:@"There was an error processing your payment information" withTitle:@"Error"];
    }];
    
   }


@end
