//
//  BankTransferViewController.m
//  AppDonator
//
//  Created by Predrag Samardzic on 23/08/15.
//  Copyright (c) 2015 pedja. All rights reserved.
//

#import "BankTransferViewController.h"

@interface BankTransferViewController ()
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *sortCodeTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *accountNumberTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sortCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *accountNumberLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *scrollContentView;

@end

@implementation BankTransferViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.nameLabel.text = self.name;
    self.sortCodeLabel.text = self.sortCode;
    self.accountNumberLabel.text = self.accountNumber;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self.scrollView setContentSize:self.scrollContentView.frame.size];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
