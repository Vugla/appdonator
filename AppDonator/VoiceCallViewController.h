//
//  VoiceCallViewController.h
//  AppDonator
//
//  Created by Predrag Samardzic on 23/08/15.
//  Copyright (c) 2015 pedja. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VoiceCallViewController : UIViewController
@property NSString* name;
@property NSString* phoneNumber;
@end
